export class Player{
  points = 0
  round = 1
  roundThrows = 0

  getPoints(){
      return this.points
  }

  getRound(){
      return this.round
  }

  throw(bolos: number){
    
    this.points += bolos
    this.roundThrows += 1

    if (this.round === 10) return "Game finished"

    if(this.roundThrows === 2 || this.strike(bolos)){
      this.nextRound()
      return "Next round"
    }

    return "Next throw"
  }

  strike(bolos: number){
    return bolos === 10
  }

  nextRound(){
    this.round += 1
    this.roundThrows = 0
  }
}