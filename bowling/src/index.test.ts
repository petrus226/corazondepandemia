import {Player} from "./index";

describe("A bowling player", () => {
  it("has points", () => {
    const player = new Player
    
    const points = player.getPoints()

    expect(points).toEqual(0);
  })

  it("gets points when throw bolos", () => {
    const player = new Player

    player.throw(1)
    player.throw(2)
    
    const points = player.getPoints()
    expect(points).toEqual(3);
  })

  describe("Rounds", ()=>{

      it("starts at 1", () => {
        const player = new Player
        
        const round = player.getRound()

        expect(round).toEqual(1);
      })

      it("keeps the same round when throws one time", () => {
        const player = new Player
        
        const response = player.throw(2)

        const round = player.getRound()
        expect(round).toEqual(1);
        expect(response).toEqual("Next throw");

      })

      it("advance when throw two times", () => {
        const player = new Player

        player.throw(1)
        const response = player.throw(2)
        
        const round = player.getRound()
        expect(round).toEqual(2);
        expect(response).toEqual("Next round");
      })

      it("advance when throw 10 bolos", () => {
        const player = new Player

        const response = player.throw(10)
        
        const round = player.getRound()
        expect(round).toEqual(2);
        expect(response).toEqual("Next round");

      })

      it("finist at 10", () => {
        const player = new Player
        for(let i = 0; i<9; i++){
            player.throw(10)
        }

        const response = player.throw(10)
        
        const round = player.getRound()
        expect(round).toEqual(10);
        expect(response).toEqual("Game finished");
      })
  })
})
